import {CanActivate, ActivatedRoute, Router} from  '@angular/router'
import {Injectable} from '@angular/core'
import {LoginService} from './login/login.service'
/*
 * LoggedInGuard
 * Guard para não acessar páginas sem estar logado
 * 
 */
@Injectable()
export class LoggedInGuard implements CanActivate {

    constructor (private route:ActivatedRoute,
                 private router:Router,
                 private loginService: LoginService){

    }

    //metodo que retorna true se usuario está logado
    canActivate(): boolean {        
        if(this.loginService.isLoogedIn())
        {
            return true
        }
        else
            this.router.navigate(['login'])
        return false
    }
}