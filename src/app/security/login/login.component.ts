import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms'
import {Router} from '@angular/router'
import {LoginService} from './login.service'
import {Usuario} from './usuario.model'


@Component({
  selector: 'mt-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup

  constructor(private fb: FormBuilder, 
              private loginService: LoginService,
              private router: Router) { }

  //inicia o formulario com as validacoes
  ngOnInit() {
    this.loginForm = this.fb.group({
      email : this.fb.control('',[Validators.required, Validators.email]),
      password : this.fb.control('',[Validators.required])
    })
  }
  
  //chama o servićo de login
  login(){
    this.loginService.login(this.loginForm.value.email, this.loginForm.value.password)
                    .subscribe(usuario=> 
                                    this.router.navigate(['usuarios']),
                                response => //HttpErrorResponse
                                    alert(response.error.message))
  }

}
