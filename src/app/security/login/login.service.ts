import {Injectable} from '@angular/core'
import {JwtHelper} from 'angular2-jwt'
import {HttpClient, HttpParams} from '@angular/common/http'
import {Observable} from 'rxjs/Observable'
import {Router} from  '@angular/router'

import 'rxjs/add/operator/do'

import {UC_API} from '../../app.api'
import {Usuario} from './usuario.model'

/*
 *  LoginService
 *  gerencia o login e o usuario logado
 *  assim como guarda o valor de retorno da paginacao * 
 */
@Injectable()
export class LoginService {

    //security.login.usuario.model - objeto retornado do login que contem o token
    usuario: Usuario
    // pagina para retornar na página que saiu para cadastrar,editar ou outro link
    pagina: string

    constructor (private router:Router,
                private http: HttpClient){

    }

    //servico que chama o post do login passando email e senha no Body
    login(email:string, senha:string) : Observable<Usuario> {
        return this.http.post<Usuario>(`${UC_API}/login`,
        {email:email, senha:senha}
    //    ,{params: new HttpParams().set("email",email).set("senha",senha)}
        ).do(usuario => this.usuario = usuario)
    }

    //faz o logout e volta para página de login inicial
    logout(){
        this.usuario = undefined
        this.pagina = undefined
        this.router.navigate(['login'])
    }

    // verifica se tem usuario logado
    isLoogedIn(): boolean {
        return this.usuario !== undefined
    }

    /*
     * verifica se usuario logado tem permissao passando todas permissoes
     * do sistema e conferindo pelas permissoes do token
     */
    userHasPermissions(permissions : string[]):boolean{
        
        let jwtHelper = new JwtHelper();
        let permissoes = jwtHelper.decodeToken(this.usuario.accessToken).permissoes
        //console.log(permissoes)
        //console.log(permissions)
        for(let permissao of permissions)
            if(permissoes.indexOf(permissao) == -1)
                return false
        
        return true
    }

    //verifica se usuario logado tem papel administrador
    isAdmin(): boolean {
        let jwtHelper = new JwtHelper();
        return jwtHelper.decodeToken(this.usuario.accessToken).role ==="administrador"
    }

    //verifica se usuario logado tem papel usuario
    isUsuario(): boolean {
        let jwtHelper = new JwtHelper();
        return jwtHelper.decodeToken(this.usuario.accessToken).role ==="usuario"
    }

    //verifica se usuario logado tem papel cadastro
    isCadastro(): boolean {
        let jwtHelper = new JwtHelper();
        return jwtHelper.decodeToken(this.usuario.accessToken).role ==="cadastro"
    }
    
    //retorna o Id do usuario logado
    getId(): string {
        let jwtHelper = new JwtHelper();
        return jwtHelper.decodeToken(this.usuario.accessToken).id
    }  

    //retorna o email/login do usuario logado
    getSub(): string {
        let jwtHelper = new JwtHelper();
        return jwtHelper.decodeToken(this.usuario.accessToken).sub
    }       

    //retorna se esta tentando editar o proprio perfil
    editarPerfil(id : string):boolean{        
        let jwtHelper = new JwtHelper();
        return jwtHelper.decodeToken(this.usuario.accessToken).id ===id
    }    


}