import {Routes} from '@angular/router'

import {UsuariosComponent} from './usuarios/usuarios.component'
import {SobreComponent} from './sobre/sobre.component'
import {UsuarioFormComponent} from './usuario-form/usuario-form.component'
import {LoginComponent} from './security/login/login.component'
import {LoggedInGuard} from './security/loggedin.guard';

export const ROUTES: Routes=[
    {path:'', component:UsuariosComponent, canActivate: [LoggedInGuard]},
    {path:'usuarios', component:UsuariosComponent, canActivate: [LoggedInGuard]},
    {path:'usuario/:id', component:UsuarioFormComponent, canActivate: [LoggedInGuard]},
    {path:'usuario', component:UsuarioFormComponent, canActivate: [LoggedInGuard]},
    {path:'login', component:LoginComponent},
    {path:'sobre', component:SobreComponent}

]