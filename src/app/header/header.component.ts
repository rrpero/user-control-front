import { Component, OnInit } from '@angular/core';
import {LoginService} from '../security/login/login.service'


@Component({
  selector: 'uc-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }

}
