import {Injectable} from '@angular/core'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import {Observable} from 'rxjs/Observable'
import {HttpClient, HttpParams,HttpHeaders} from '@angular/common/http'
import {Usuario} from './usuario/usuario.model'
import {Papel} from './papel/papel.model'

import {LoginService} from '../security/login/login.service'
import {UC_API} from '../app.api'
import {ErrorHandler} from '../app.error-handler'

/*
 * UsuariosService
 * Chama os servicos para criar, ler, editar e remover
 */ 
@Injectable()
export class UsuariosService{


    constructor(private http: HttpClient,
                private loginService: LoginService){

    }

    //retorna a lista de usuarios paginada
    usuarios(pagina:string = "0", tamanhoPagina:string = "5" ): Observable<any> {
        if(this.loginService.isLoogedIn()){
            let headers = new HttpHeaders().set('content-type', 'application/json')
                                        .set('Authorization',`Bearer ${this.loginService.usuario.accessToken}`);
            let params = new HttpParams().set("pagina",pagina)
            .set("tamanhoPagina",tamanhoPagina)
            return this.http.get(`${UC_API}/usuarios`, {params:params,headers:headers})
        }
    }

    //pega os papeis
    papeis(): Observable<Papel[]> {
        if(this.loginService.isLoogedIn()){
            let headers = new HttpHeaders().set('content-type', 'application/json')
                                        .set('Authorization',`Bearer ${this.loginService.usuario.accessToken}`);

            return this.http.get<Papel[]>(`${UC_API}/papeis`, {headers:headers})
        }
    }    

    // pega os dados de um usuario
    usuarioById(id : string): Observable<Usuario> {
        if(this.loginService.isLoogedIn()){
            let headers = new HttpHeaders().set('content-type', 'application/json')
                                        .set('Authorization',`Bearer ${this.loginService.usuario.accessToken}`);

            return this.http.get<Usuario>(`${UC_API}/usuarios/${id}`, {headers:headers})
        }
    }

    // cadastra um usuario
    criaUsuario(usuario){
        if(this.loginService.isLoogedIn()){
            let headers = new HttpHeaders().set('content-type', 'application/json')
                                        .set('Authorization',`Bearer ${this.loginService.usuario.accessToken}`);

            return this.http.post(`${UC_API}/usuarios`, JSON.stringify(usuario),{headers:headers })
        }
          
      }
    
    // edita um usuario
    editaUsuario(usuario){
        if(this.loginService.isLoogedIn()){
            let headers = new HttpHeaders().set('content-type', 'application/json')
                                        .set('Authorization',`Bearer ${this.loginService.usuario.accessToken}`);

            return this.http.put(`${UC_API}/usuarios/${usuario.id}`,JSON.stringify(usuario), {headers:headers  })
        }
    }
    
    // remove um usuario
    removerUsuario(id): Observable<any>{
        if(this.loginService.isLoogedIn()){
            let headers = new HttpHeaders().set('content-type', 'application/json')
                                        .set('Authorization',`Bearer ${this.loginService.usuario.accessToken}`);

            return this.http.delete(`${UC_API}/usuarios/${id}`,{headers:headers})
        }
    }    


}