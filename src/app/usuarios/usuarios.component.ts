import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {Usuario} from './usuario/usuario.model'
import {UsuariosService} from './usuarios.service'
import {LoginService} from '../security/login/login.service'

@Component({
  selector: 'uc-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
/*
 * UsuariosComponent
 * Gerencia a lista de usuários e sua paginacão
 */
export class UsuariosComponent implements OnInit {

  usuarios: Usuario[]
  last: boolean
  totalElements: string
  totalPages= []
  sort: string
  first: boolean
  numberOfElements:string
  size: string
  number: string

  constructor(private usuariosService: UsuariosService,
              private loginService: LoginService,
              private router:Router) { }

  ngOnInit() {
    
    //pega a página
    this.loginService.pagina = this.loginService.pagina ? this.loginService.pagina : '0'
    //carrega na página
    this.irParaPagina(this.loginService.pagina)     
  }

  // chama o servico de remover usuário e atualiza a lista de usuarios
  removerUsuario(usuario){
    if (confirm("Tem certeza que deseja remover o usuário " + usuario.email + "?")) {
      var index = this.usuarios.indexOf(usuario);
      this.usuarios.splice(index, 1);

      this.usuariosService.removerUsuario(usuario.id)
        .subscribe(success=>this.irParaPagina(this.number),
          err => {
            alert("Não foi possível remover.");
            // Reverter
            this.usuarios.splice(index, 0, usuario);
          });
          
    }
    
  }

  // funcão que manda a paginacao para a pagina e carrega os usuarios dela
  irParaPagina(pagina:string){ 
    this.loginService.pagina = pagina   
    this.usuariosService.usuarios(pagina)
    .subscribe(usuarios => {
                            this.usuarios = usuarios.content
                            this.last= usuarios.last
                            this.totalElements = usuarios.totalElements
                            //fazendo array de numeros de paginas
                            this.totalPages = Array(usuarios.totalPages).fill(0).map((x,i)=>i)
                            this.sort = usuarios.sort
                            this.first = usuarios.first
                            this.numberOfElements = usuarios.numberOfElements
                            this.size = usuarios.size
                            this.number = usuarios.number
                            this.router.navigate(['usuarios'])                           
                        })
                           
  }

}
