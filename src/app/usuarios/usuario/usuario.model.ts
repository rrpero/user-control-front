import {Papel} from '../papel/papel.model'

export class Usuario {
    
        id: string
        nome: string
        sobrenome: string
        email: string
        idPapel: String  
        papel:Papel      

        constructor(){
            this.papel = new Papel()
        }
    
}