import { Component, OnInit, Input } from '@angular/core';
import { Usuario } from './usuario.model'
import {LoginService} from '../../security/login/login.service'

@Component({
  selector: 'uc-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  @Input() usuario : Usuario

  constructor(private loginService: LoginService) { 

  }

  ngOnInit() {
  }



}
