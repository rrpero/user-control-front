import { Component, OnInit ,Input} from '@angular/core'
import {ActivatedRoute, Router} from '@angular/router'
import {FormGroup, FormBuilder,Validators,FormControl,ValidatorFn} from '@angular/forms'
import {LoginService} from '../security/login/login.service'
import {UsuariosService} from '../usuarios/usuarios.service'
import {Usuario} from '../usuarios/usuario/usuario.model'
import {Papel} from '../usuarios/papel/papel.model'
import {PasswordValidators} from './password.validators'

@Component({
  selector: 'uc-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.css']
})

/*
 * UsuarioFormComponent
 * Gerencia o formulario de cadastrar ou editar
 */
export class UsuarioFormComponent implements OnInit {
  title: string
  titleButton: string
  usuarioForm : FormGroup
  usuario : Usuario = new Usuario()
  papeis: Papel[] 

  
  constructor(private usuariosService: UsuariosService,
              private loginService: LoginService,
              private route : ActivatedRoute,
              private router: Router,
              private fb: FormBuilder) { 
      
      var id = this.route.params.subscribe(params => {
        var id = params['id'];
        // Validacao para cadastrar
        if(!id){
          this.usuarioForm = this.fb.group({
            nome : this.fb.control('',[Validators.required]),
            sobrenome : this.fb.control('',[Validators.required]),
            id : this.fb.control('',[]),
            senha : this.fb.control('',[Validators.required]),
            email : this.fb.control('',[Validators.required, Validators.email]),
            idPapel : this.fb.control('',[Validators.required]),
            confirmarSenha : this.fb.control('',[Validators.required])},
            {
              validator : PasswordValidators.senhaConfirmada
            }
    
          )
  
        }// Validacao para editar
        else{
          this.usuarioForm = this.fb.group({
            nome : this.fb.control('',[Validators.required]),
            sobrenome : this.fb.control('',[Validators.required]),
            id : this.fb.control('',[]),
            senha : this.fb.control(''),
            email : this.fb.control('',[Validators.required, Validators.email]),
            idPapel : this.fb.control('',[Validators.required]),
            confirmarSenha : this.fb.control('')},
            {
              validator : PasswordValidators.senhaConfirmada
            }
    
          )
        }
      })

  }

  ngOnInit() {
    var id = this.route.params.subscribe(params => {
      var id = params['id'];

      this.title = id ? 'Editar Usuário' : 'Novo Usuário';
      this.titleButton = id ? 'Editar' : 'Criar';
      
      this.usuariosService.papeis()
      .subscribe(papeis => this.papeis = papeis)     

      //se for cadastrar não pega os dados do usuário
      if (!id)
        return;
      
      this.usuariosService.usuarioById(id)
        .subscribe(
          usuario => this.usuario = usuario,
          response => {
            if (response.status == 404) {
              this.router.navigate(['NotFound']);
            }
          }
        );
    });

  }

  /*
   * faz o submit do formulario e verifica se manda editar ou cadastrar
   * se o email for diferente faz logout para relogar
   */
  salvar() {
    var result,
    usuarioValue = this.usuarioForm.value;

    if (usuarioValue.id){
      result = this.usuariosService.editaUsuario(usuarioValue)
      result.subscribe(data =>
         {
           if(this.loginService.getSub() != this.usuarioForm.get('email').value)
               this.loginService.logout()
         })

      //console.log(this.usuarioForm)
    } else {
      result = this.usuariosService.criaUsuario(usuarioValue);
      //console.log(usuarioValue)
    }

    result.subscribe(data => this.router.navigate(['usuarios']));
  }

  // retorna campo senha
  get senha(){
      return this.usuarioForm.get('senha')
  }

  // retorna campo confirmar senha
  get confirmarSenha(){
    return this.usuarioForm.get('confirmarSenha')
  }



}



