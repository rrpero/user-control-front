import {AbstractControl} from '@angular/forms'
/*
 * PasswordValidators
 * Validator para confirmar senha ao cadastrar e se o usuário quiser mudar a senha ao editar
 */
export class PasswordValidators{

    static senhaConfirmada(control:AbstractControl){
        let senha = control.get('senha')
        let confirmarSenha = control.get('confirmarSenha')

        if(senha.value!==confirmarSenha.value && !(senha.value == undefined && confirmarSenha.value == "")){
            //console.log("Passou?")
            return { senhaConfirmada : true }
        }
        else
            //console.log("Nao Passou?")
        return null
    }
}