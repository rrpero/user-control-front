import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {ROUTES} from './app.routes'

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from  '@angular/common/http'
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UsuarioComponent } from './usuarios/usuario/usuario.component';
import { UsuariosService } from './usuarios/usuarios.service';
import { LoginService } from './security/login/login.service';
import { PapelComponent } from './usuarios/papel/papel.component'
import { UsuarioFormComponent } from './usuario-form/usuario-form.component'
import { LoginComponent } from './security/login/login.component';
import { LoggedInGuard } from './security/loggedin.guard';
import { SobreComponent } from './sobre/sobre.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UsuariosComponent,
    UsuarioComponent,
    PapelComponent,
    UsuarioFormComponent,
    LoginComponent,
    SobreComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(ROUTES),    
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [UsuariosService,
              LoginService,
            LoggedInGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
