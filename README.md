# UserControl - Controle de Usuarios, com Papeis e suas Permissões

## 1. UserControl - Backend: Feito com SpringBoot e banco em memória criado pela modelagem das entities

### Clonando o Repositório

`git clone  https://rrpero@bitbucket.org/rrpero/user-control-api.git`

### Rodando com o Maven

`cd user-control-api`

`./mvnw spring-boot:run`

## 2. UserControl - Frontend: Feito com Angular 4 e Bootstrap

### Clonando o Repositório

`git clone  https://rrpero@bitbucket.org/rrpero/user-control-front.git`

### Instalando as Dependências

`cd user-control-front`

`npm install`

### Inicializando o Servidor

`ng serve`

## 3. Utilizando o UserControl

### Vá até o navegador e entre no endereço 

`http://localhost:4200`

### Usuários para teste

#### Papel Administrador - Pode cadastrar, consultar, editar e remover(menos ele mesmo) usuários

login: `benjaminlinus@gmail.com`

senha: `senhabenjaminlinus`

#### Papel Cadastro - Pode cadastrar e consultar porém só pode editar o próprio perfil

login: `kateausten@gmail.com`

senha: `senhakateausten`

#### Papel Usuário - Pode consultar usuários e editar o próprio perfil

login: `franklapidus@gmail.com`

senha: `senhafranklapidus`